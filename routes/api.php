<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\MemorandumController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PromotionalMaterialController;
use App\Http\Controllers\PurchaseOrderController;
use App\Http\Controllers\PurchaseOrderDetailController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SaleExpenseController;
use App\Http\Controllers\SaleExpenseProductController;
use App\Http\Controllers\TargetAssignmentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkPlanController;
use App\Http\Controllers\ZoneController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\DistributorLedger;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::group(['middleware' => ['roletype:'.Roles::ADMIN]], function() {
        Route::get('/user',  function (Request $request) {
            return $request->user();
        });
    });
    Route::group(['middleware' => ['roletype:'.Roles::ADMIN.','.Roles::DISTRIBUTER]], function() {
        Route::post('/purchaseOrder/update', [PurchaseOrderController::class, 'update']);
    });
    Route::group(['middleware' => ['roletype:'.Roles::ADMIN.','.Roles::EMPLOYEE.','.Roles::MANAGER]], function() {
        Route::post('/workPlan', [WorkPlanController::class, 'index']);
        Route::post('/workPlan/workPlanDetails', [WorkPlanController::class, 'getWorkPlanDetails']);
        Route::post('/workPlan/get/{id}', [WorkPlanController::class, 'show']);
        Route::post('/workPlan/create', [WorkPlanController::class, 'store']);
        Route::post('/workPlan/update', [WorkPlanController::class, 'update']);
    });
    Route::post('/logout', [AuthController::class, 'logout']);

    // Product Apis
    Route::post('/product', [ProductController::class, 'index']);
    Route::post('/product/create', [ProductController::class, 'store']);
    Route::post('/product/update', [ProductController::class, 'update']);
    Route::post('/product/get/{id}', [ProductController::class, 'show']);

    // Zone Apis
    Route::post('/zone', [ZoneController::class, 'index']);
    Route::post('/zone/create', [ZoneController::class, 'store']);
    Route::post('/zone/update', [ZoneController::class, 'update']);
    Route::post('/zone/get/{id}', [ZoneController::class, 'show']);

    // Company Apis
    Route::post('/company', [CompanyController::class, 'index']);
    Route::post('/company/create', [CompanyController::class, 'store']);
    Route::post('/company/update', [CompanyController::class, 'update']);
    Route::post('/company/get/{id}', [CompanyController::class, 'show']);

    // Employee Apis
    Route::post('/employee', [EmployeeController::class, 'index']);
    Route::post('/employee/update', [EmployeeController::class, 'update']);
    Route::post('/employee/get/{id}', [EmployeeController::class, 'show']);

    // Role Apis
    Route::post('/role', [RoleController::class, 'index']);
    Route::post('/role/create', [RoleController::class, 'store']);
    Route::post('/role/update', [RoleController::class, 'update']);
    Route::post('/role/get/{id}', [RoleController::class, 'show']);

    // Promotional  Apis
    Route::post('/promotionalMaterial', [PromotionalMaterialController::class, 'index']);
    Route::post('/promotionalMaterial/create', [PromotionalMaterialController::class, 'store']);
    Route::post('/promotionalMaterial/update', [PromotionalMaterialController::class, 'update']);
    Route::post('/promotionalMaterial/get/{id}', [PromotionalMaterialController::class, 'show']);

    // Memorandum Apis
    Route::post('/memorandum', [MemorandumController::class, 'index']);
    Route::post('/memorandum/create', [MemorandumController::class, 'store']);
    Route::post('/memorandum/update', [MemorandumController::class, 'update']);
    Route::post('/memorandum/get/{id}', [MemorandumController::class, 'show']);

    // Sale Expense Apis
    Route::post('/salesExpense', [SaleExpenseController::class, 'index']);
    Route::post('/salesExpense/create', [SaleExpenseController::class, 'store']);
    Route::post('/salesExpense/update', [SaleExpenseController::class, 'update']);
    Route::post('/salesExpense/get/{id}', [SaleExpenseController::class, 'show']);

    // Target Assignment Apis
    Route::post('/targetAssignment', [TargetAssignmentController::class, 'index']);
    Route::post('/targetAssignment/create', [TargetAssignmentController::class, 'store']);
    Route::post('/targetAssignment/update', [TargetAssignmentController::class, 'update']);
    Route::post('/targetAssignment/get/{id}', [TargetAssignmentController::class, 'show']);
    Route::post('/targetAssignment/targetAssignmentDetail', [TargetAssignmentController::class, 'getTargetAssignmentDetail']);

    // Sale Expense Product Apis
    Route::post('/salesExpenseProduct', [SaleExpenseProductController::class, 'index']);

    // Purchase Order Apis
    Route::post('/purchaseOrder', [PurchaseOrderController::class, 'index']);
    Route::post('/purchaseOrder/create', [PurchaseOrderController::class, 'store']);
    Route::post('/purchaseOrder/get/{id}', [PurchaseOrderController::class, 'show']);

    // Purchase Order Apis
    Route::post('/purchaseOrderDetail', [PurchaseOrderDetailController::class, 'index']);

    // User Apis
    Route::post('/user', [UserController::class, 'index']);

    // Doctor Apis
    Route::post('/doctor', [DoctorController::class, 'index']);
    Route::post('/doctor/create', [DoctorController::class, 'store']);
    Route::post('/doctor/get/{id}', [DoctorController::class, 'show']);

    // Distributor Ledger Apis
    Route::post('/distributorLedger', [DistributorLedger::class, 'index']);
    Route::post('/distributorLedger/create', [DistributorLedger::class, 'store']);
    Route::post('/distributorLedger/get/{id}', [DistributorLedger::class, 'show']);
    
    // Chart Apis
    Route::post('/productAnalysisByTargetChart', [ChartController::class, 'getProductAnalysisByTarget']);
    Route::post('/productAnalysisByProductChart', [ChartController::class, 'getProductAnalysisByProduct']);

});