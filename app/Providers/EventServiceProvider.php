<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\PurchaseOrder;
use App\Models\WorkPlan;
use App\Observers\CompanyObserver;
use App\Observers\PurchaseOrderObserver;
use App\Observers\WorkPlanObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Company::observe(CompanyObserver::class);
        PurchaseOrder::observe(PurchaseOrderObserver::class);
        WorkPlan::observe(WorkPlanObserver::class);
    }
}
