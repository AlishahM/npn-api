<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleExpenseProduct extends Model
{
    use SoftDeletes;

    protected $table = 'sale_expense_products';
    
    protected $fillable = [
        'sale_expense_id',
        'product_id',
        'name',
        'price',
        'product_unit'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Relations
    public function sale_expense(){
        return $this->belongsTo("App\Models\SaleExpense");
    }
}
