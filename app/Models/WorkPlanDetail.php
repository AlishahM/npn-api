<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkPlanDetail extends Model
{
    use SoftDeletes;

    protected $table = 'work_plan_details';
    
    protected $fillable = [
        'id', 'work_plan_id', 'doctor', 'date', 'revised_date', 'work_type', 'morning_area', 'morning_time', 'evening_area', 'evening_time'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;
}
