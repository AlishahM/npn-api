<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    /**
     * Role Type constants.
     *
     * @var string
     */
    public const ADMIN = 1;
    public const EMPLOYEE = 2;
    public const DISTRIBUTER = 3;
    public const MANAGER = 4;


    use SoftDeletes;

    protected $table = 'roles';
    
    protected $fillable = [
        'role'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;
}
