<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DistributorLedgers extends Model
{
    use HasFactory;

    protected $table = 'distributor_ledgers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'zone',
        'distributorName',
        'quantity',
        'totalAmount',
        'dopr',
        'status'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];
}
