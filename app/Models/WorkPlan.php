<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkPlan extends Model
{
    use SoftDeletes;

    protected $table = 'work_plans';
    
    protected $fillable = [
        'id', 'plan_no', 'user_id', 'role_type', 'from_date', 'to_date', 'month', 'year', 'isPresent'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    protected $casts = [
        'isPresent' => 'boolean'
    ];

    //Relations
    public function work_plan_details(){
        return $this->hasMany("App\Models\WorkPlanDetail");
    }
    public function user(){
        return $this->belongsTo("App\Models\User");
    }
}
