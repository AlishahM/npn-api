<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrderDetail extends Model
{
    use SoftDeletes;

    protected $table = 'purchase_order_details';
    
    protected $fillable = [
        'purchase_order_id',
        'product_id',
        'product_name',
        'order_unit',
        'price',
        'amount'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    //Relations
    public function purchase_order(){
        return $this->belongsTo("App\Models\PurchaseOrder");
    }
    public function company(){
        return $this->belongsTo("App\Models\Company");
    }
    public function user(){
        return $this->belongsTo("App\Models\User");
    }
}
