<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TargetAssignmentDetail extends Model
{
    use SoftDeletes;

    protected $table = 'target_assignment_details';
    
    protected $fillable = [
        'target_assignment_id',
        'product_id',
        'product_name',
        'employee_id',
        'employee_name',
        'order_unit'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;
}
