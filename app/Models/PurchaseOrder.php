<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use SoftDeletes;

    protected $table = 'purchase_orders';
    
    protected $fillable = [
        'po_no',
        'user_id',
        'zone_id',
        'distributer_area',
        'order_date',
        'company_id',
        'is_accepted'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    //Relations
    public function company(){
        return $this->belongsTo("App\Models\Company");
    }
    public function user(){
        return $this->belongsTo("App\Models\User");
    }
    public function zone(){
        return $this->belongsTo("App\Models\Zone");
    }
    public function product_units(){
        return $this->hasMany("App\Models\PurchaseOrderDetail");
    }
}
