<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TargetAssignment extends Model
{
    use SoftDeletes;

    protected $table = 'target_assignments';
    
    protected $fillable = [
        'user_id',
        'role_type',
        'company_id',
        'zone_id',
        'year',
        'month'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Relations
    public function user(){
        return $this->belongsTo("App\Models\User");
    }
    public function company(){
        return $this->belongsTo("App\Models\Company");
    }
    public function zone(){
        return $this->belongsTo("App\Models\Zone");
    }
    public function target_assignment_details(){
        return $this->hasMany("App\Models\TargetAssignmentDetail");
    }
}
