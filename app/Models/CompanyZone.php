<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyZone extends Model
{
    use SoftDeletes;

    protected $table = 'companies_zones';
    
    protected $fillable = [
        'company_id',
        'zone_id'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Relations ...
    public function zone(){
        return $this->belongsTo("App\Models\Zone", "zone_id", "id");
    }
    public function companies(){
        return $this->belongsTo("App\Models\Company", "company_id", "id");
    }
}
