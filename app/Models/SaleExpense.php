<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleExpense extends Model
{
    use SoftDeletes;

    protected $table = 'sale_expenses';
    
    protected $fillable = [
        'user_id',
        'role_type',
        'company_id',
        'zone_id',
        'total_unit_sold',
        'total_sale_value',
        'total_month_expense',
        'total_annual_expense',
        'total_claim_expense',
        'total_promotional_expense'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    //Relations
    public function company(){
        return $this->belongsTo("App\Models\Company");
    }
    public function user(){
        return $this->belongsTo("App\Models\User");
    }
    public function product_units(){
        return $this->hasMany("App\Models\SaleExpenseProduct");
    }
}
