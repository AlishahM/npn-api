<?php 

namespace App\Helpers;
use Illuminate\Support\Arr;

class ArrayHelper
{
    public static function dotToArray($dotArray)
    {
        $errors = [];
        foreach ($dotArray as $key => $value) {
            Arr::set($errors, $key, $value);
        }
        return $errors;
    }
}