<?php

namespace App\Helpers;

/**
 * Database Query builder.
 *
 * sample query: (C1 AND (C2 OR C3)) OR C4
 * 
 * [
 *    {
 *      "lop": "AND,
 *       "sub": {
 *               "flt":
 *                 [
 *                 {"lop": "AND", "col": xyz, "cop": "=", "val" 123},
 *                  {
 *                     "lop": "AND",
 *                     "sub": {
 *                            "flt": [
 *                                    {"lop": "AND", "col": xyz, "cop": "=", "val" 123},
 *                                    {"lop": "OR", "col": xyz, "cop": "=", "val" 123}
 *                                   ]
 *                             } 
 *                 }
 *                 ]
 *            }
 *     },
 *     {
 *        "lop": "OR",
 *        "col": xyz,
 *        "cop": "=",
 *        "val" 123
 *     }
 * ]
 * 
 * "lop" in first array of filter or "flt" is ignored
 * @return void
 */
class DatabaseQueryHelper
{
    public static function filter(&$query, &$filters)
    {
        if (empty($filters) || !is_array($filters))
            return $query;

        return $query->whereRaw(self::getWhere($filters));
    }



    public static function getWhere($filters)
    {
        $query = "";
        foreach ($filters as $index => $filter) {
            // append logical operator, escape for fist condition
            if ($index != 0) {
                if (isset($filter['lop']))
                    $query .= ' ' . $filter['lop'] . ' ';
            }

            if (isset($filter['cop'])) {
                if ($filter['cop'] == 'is')
                    $query .= "${filter['col']} is ${filter['val']}";
                else
                    $query .= "${filter['col']} ${filter['cop']} '${filter['val']}'";
            }

            if (isset($filter['sub']['flt'])) {
                // recursive call for sub fitlers
                $query .= ' ( ' . self::getWhere($filter['sub']['flt']) . ' ) ';
            }
        }
        return $query;
    }
}
