<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\SaleExpense;
use App\Models\SaleExpenseProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SaleExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['sale_expenses.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = SaleExpense::with(["company", "user", "user.zone", "product_units"])->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $saleExpenseRules = [
            'user_id' => 'required',
            'company_id' => 'required',
            'total_unit_sold' => 'required',
            'total_sale_value' => 'required',
            'total_month_expense' => 'required',
            'total_annual_expense' => 'required',
            'total_claim_expense' => 'required',
            'total_promotional_expense' => 'required',
            'product_units' => 'required',
            'product_units.*.product_id' => 'required',
            'product_units.*.name' => 'required',
            'product_units.*.price' => 'required',
            'product_units.*.product_unit' => 'required',
        ];
    
        $errorMessages = [
            'user_id.required' => 'User ID Name is Required.',
            'company_id.required' => 'Company ID is Required.',
            'total_unit_sold.required' => 'Total Unit Sold is Required.',
            'total_sale_value.required' => 'Total Sale Value is Required.',
            'total_month_expense.required' => 'Total Month Expense is Required.',
            'total_annual_expense.required' => 'Total Annual Expense is Required.',
            'total_claim_expense.required' => 'Total Claim Expense is Required.',
            'total_promotional_expense.required' => 'Total Promotional Expense is Required.',
            'product_units.required' => 'Product Units is Required.',
            'product_units.*.product_id' => 'Product ID is Required',
            'product_units.*.name' => 'Product Name is Required',
            'product_units.*.price' => 'Product Price is Required',
            'product_units.*.product_unit' => 'Product Unit Qty is Required',
        ];

        $saleExpenseValidator = Validator::make($reqParams, $saleExpenseRules, $errorMessages);
        if ($saleExpenseValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($saleExpenseValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $saleExpense = new SaleExpense($reqParams);
            $saleExpense->save();
            $saleExpenseProductIds = [];
            if (array_key_exists('product_units', $reqParams)) {
                foreach ($reqParams['product_units'] as $key => $item) {
                    // new designation
                    $saleExpenseProduct = null;
                    if (empty($item['id'])) {
                        $saleExpenseProduct = new SaleExpenseProduct($item);
                        $saleExpenseProduct->sale_expense_id = $saleExpense->id;
                        $saleExpenseProduct->save();
                    } else {
                        $saleExpenseProduct = SaleExpenseProduct::find($item['id']);
                        if ($saleExpenseProduct) {
                            
                            $saleExpenseProduct->update($item);
                        }
                    }
                    $saleExpenseProductIds[] = $saleExpenseProduct->id;
                }
            }
            if(count($saleExpenseProductIds) > 0)
            {
                DB::statement("UPDATE sale_expense_products SET deleted_at='".Carbon::now()->format("Y-m-d H:i:s")."' WHERE sale_expense_id={$saleExpense->id} && id not in (". implode( ',', $saleExpenseProductIds).")");
            }
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [];
        $saleExpense = SaleExpense::find($id);
        if (!$saleExpense) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $saleExpense;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->all();

        $saleExpenseRules = [
            'id' => 'required',
            'user_id' => 'required',
            'company_id' => 'required',
            'total_unit_sold' => 'required',
            'total_sale_value' => 'required',
            'total_month_expense' => 'required',
            'total_annual_expense' => 'required',
            'total_claim_expense' => 'required',
            'total_promotional_expense' => 'required',
            'product_units' => 'required',
        ];
    
        $errorMessages = [
            'id.required' => 'ID is Required.',
            'user_id.required' => 'User ID Name is Required.',
            'company_id.required' => 'Company ID is Required.',
            'total_unit_sold.required' => 'Total Unit Sold is Required.',
            'total_sale_value.required' => 'Total Sale Value is Required.',
            'total_month_expense.required' => 'Total Month Expense is Required.',
            'total_annual_expense.required' => 'Total Annual Expense is Required.',
            'total_claim_expense.required' => 'Total Claim Expense is Required.',
            'total_promotional_expense.required' => 'Total Promotional Expense is Required.',
            'product_units.required' => 'Product Units is Required.',
        ];

        $saleExpenseValidator = Validator::make($reqParams, $saleExpenseRules, $errorMessages);

        if ($saleExpenseValidator->fails()) {
            $response['message']['errors'] = ArrayHelper::dotToArray($saleExpenseValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            SaleExpense::find($reqParams['id'])->update($reqParams);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $e) {
            $response['message']['errors']['general'][] = [$e->getMessage()];
            return response()->json($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
