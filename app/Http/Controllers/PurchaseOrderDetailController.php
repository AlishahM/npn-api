<?php

namespace App\Http\Controllers;

use App\Helpers\DatabaseQueryHelper;
use App\Models\PurchaseOrderDetail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PurchaseOrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['purchase_order_details.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = PurchaseOrderDetail::with(["purchase_order:id,po_no,company_id,user_id,zone_id,distributer_area,order_date", "purchase_order.company:id,name", "purchase_order.user:id,name", "purchase_order.zone:id,zone"])->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }
}
