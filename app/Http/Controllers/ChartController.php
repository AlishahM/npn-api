<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductAnalysisByTarget(Request $request)
    {
        $response = [];
        extract(array_filter($request->all()));

        for ($i=0; $i < 12; $i++) { 
            $response['target_assignment'][$i] = DB::select("SELECT SUM(target_assignment_details.order_unit) as order_unit FROM target_assignment_details JOIN target_assignments ON target_assignments.id=target_assignment_details.target_assignment_id WHERE company_id='".$companyId."' AND employee_id='".$employeeId."' AND year='".$year."' AND month='".($i+1)."' GROUP BY company_id, zone_id, employee_name");
            $response['sale_expense'][$i] = DB::select("SELECT SUM(sale_expenses.total_unit_sold) as total_unit_sold FROM sale_expenses WHERE company_id='".$companyId."' AND user_id='".$employeeId."' AND EXTRACT(YEAR FROM created_at)='".$year."' AND EXTRACT(MONTH FROM created_at) ='".($i+1)."' GROUP BY company_id, zone_id, user_id");
            try {
                if ($response['target_assignment'][$i][0]->order_unit) {
                    $response['target_assignment'][$i] = intval($response['target_assignment'][$i][0]->order_unit);
                }
            } catch (Exception $e) {
                $response['target_assignment'][$i] = 0;
            }
            try {
                if ($response['sale_expense'][$i][0]->total_unit_sold) {
                    $response['sale_expense'][$i] = intval($response['sale_expense'][$i][0]->total_unit_sold);
                }
            } catch (Exception $e) {
                $response['sale_expense'][$i] = 0;
            }
        }
        
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductAnalysisByProduct(Request $request)
    {
        $response = [];
        extract(array_filter($request->all()));

        for ($i=0; $i < 12; $i++) { 
            $response['target_assignment'][$i] = DB::select("SELECT SUM(target_assignment_details.order_unit) as order_unit FROM target_assignment_details JOIN target_assignments ON target_assignments.id=target_assignment_details.target_assignment_id WHERE company_id='".$companyId."' AND product_id='".$productId."' AND year='".$year."' AND month='".($i+1)."' GROUP BY company_id, product_id");
            $response['sale_expense'][$i] = DB::select("SELECT SUM(sale_expense_products.product_unit) as product_unit FROM sale_expense_products JOIN sale_expenses ON sale_expense_products.sale_expense_id = sale_expenses.id WHERE company_id='".$companyId."' AND product_id='".$productId."' AND EXTRACT(YEAR FROM sale_expenses.created_at)='".$year."' AND EXTRACT(MONTH FROM sale_expenses.created_at) ='".($i+1)."' GROUP BY company_id, product_id");
            try {
                if ($response['target_assignment'][$i][0]->order_unit) {
                    $response['target_assignment'][$i] = intval($response['target_assignment'][$i][0]->order_unit);
                }
            } catch (Exception $e) {
                $response['target_assignment'][$i] = 0;
            }
            try {
                if ($response['sale_expense'][$i][0]->product_unit) {
                    $response['sale_expense'][$i] = intval($response['sale_expense'][$i][0]->product_unit);
                }
            } catch (Exception $e) {
                $response['sale_expense'][$i] = 0;
            }
        }
        
        return response()->json($response, Response::HTTP_OK);
    }
}
