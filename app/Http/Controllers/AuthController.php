<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|string',
            // 'email' => 'required|string|unique:users,email',
            'username' => 'required|string|unique:users',
            'password' => 'required|string',
            'role_type' => 'required|integer',
            'cnic' => 'required|integer',
            'designation' => 'required|string',
            'phone_number' => 'required|string',
            'address' => 'required|string',
            'salary' => 'required|integer',
            'zone_id' => 'required|integer',
            'company_id' => 'required|integer',
            'status' => 'required|boolean',
        ]);

        $user = User::create([
            'name' => $fields['name'],
            // 'email' => $fields['email'],
            'username' => $fields['username'],
            'role_type' => $fields['role_type'],
            'cnic' => $fields['cnic'],
            'designation' => $fields['designation'],
            'phone_number' => $fields['phone_number'],
            'address' => $fields['address'],
            'salary' => $fields['salary'],
            'zone_id' => $fields['zone_id'],
            'company_id' => $fields['company_id'],
            'status' => $fields['status'],
            'password' => bcrypt(($fields['password'])),
        ]);

        $token = $user->createToken('myapptoken')->plainTextToken;

        $user = User::with(['zone', 'company', 'role'])->find($user->id);

        $response = [
            'user' => $user,
            'token' => $token
        ];
        return response($response, 201);
    }

    public function login(Request $request)
    {
        $fields = $request->validate([
            // 'username' => 'required|string|unique:users,email',
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        // Check username
        $user = User::where('username', $fields['username'])->first();

        // Check Password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Login Failed'
            ], 401);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;

        $user = User::with(['zone', 'company', 'role'])->find($user->id);

        $response = [
            'user' => $user,
            'token' => $token
        ];
        return response($response, 201);
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return ['message' => 'Logged Out'];
    }
}
