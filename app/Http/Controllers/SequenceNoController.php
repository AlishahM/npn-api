<?php

namespace App\Http\Controllers;

use App\Models\SequenceNo;
use Illuminate\Http\Request;

class SequenceNoController extends Controller
{
    /**
     * Return Purchase Order No.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getPurchaseOrderNo()
    {
        return "PO".substr(str_repeat(0, 5).SequenceNo::get('purchase_order')->first()->value('purchase_order'), - 5);
    }

    /**
     * Return Purchase Order No.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getWorkPlanNo()
    {
        return "PO".substr(str_repeat(0, 5).SequenceNo::get('work_plan_no')->first()->value('work_plan_no'), - 5);
    }

    /**
     * Update a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function updatePurchaseOrderNo()
    {
        $sequenceNo =  SequenceNo::select('id','purchase_order')->get();
        $number = $sequenceNo[0]['purchase_order'];
        $sequenceNo[0]->purchase_order = $number +1;
        $sequenceNo[0]->save();
    }

    /**
     * Update a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function updateWorkPlanNo()
    {
        $sequenceNo =  SequenceNo::select('id','work_plan_no')->get();
        $number = $sequenceNo[0]['work_plan_no'];
        $sequenceNo[0]->work_plan_no = $number +1;
        $sequenceNo[0]->save();
    }
}
