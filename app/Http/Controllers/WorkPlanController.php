<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\WorkPlan;
use App\Models\WorkPlanDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WorkPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['work_plans.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = WorkPlan::with(["user", "user.zone", "work_plan_details"])->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWorkPlanDetails(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['work_plan_details.*','work_plans.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = WorkPlan::with(["user", "user.zone"])->rightjoin("work_plan_details", "work_plan_details.work_plan_id", "work_plans.id")->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $workPlanRules = [
            'user_id' => 'required',
            'role_type' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'month' => 'required',
            'year' => 'required',
            'work_plan_details' => 'required',
            'work_plan_details.*.date' => 'required',
            'work_plan_details.*.revised_date' => 'required',
            'work_plan_details.*.work_type' => 'required',
            'work_plan_details.*.doctor' => 'required',
            'work_plan_details.*.morning_area' => 'required',
            'work_plan_details.*.morning_time' => 'required',
            'work_plan_details.*.evening_area' => 'required',
            'work_plan_details.*.evening_time' => 'required'
        ];
    
        $errorMessages = [
            'user_id.required' => 'User ID Name is Required.',
            'role_type.required' => 'Role Type is Required.',
            'from_date.required' => 'From Date is Required.',
            'to_date.required' => 'To Date is Required.',
            'month.required' => 'Month is Required.',
            'year.required' => 'Year is Required.',
            'work_plan_details' => 'Work Plan Detail is Required',
            'work_plan_details.*.date' => 'Date is Required',
            'work_plan_details.*.revised_date' => 'Revised Date is Required',
            'work_plan_details.*.work_type' => 'Work Type is Required',
            'work_plan_details.*.doctor' => 'Doctor is Required',
            'work_plan_details.*.morning_area' => 'Morning Area is Required',
            'work_plan_details.*.morning_time' => 'Morning Time is Required',
            'work_plan_details.*.evening_area' => 'Evening Area is Required',
            'work_plan_details.*.evening_time' => 'Evening Time is Required'
        ];

        $workPlanValidator = Validator::make($reqParams, $workPlanRules, $errorMessages);
        if ($workPlanValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($workPlanValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $workPlan = new WorkPlan($reqParams);
            $workPlan->save();
            $workPlanDetailIds = [];
            if (array_key_exists('work_plan_details', $reqParams)) {
                foreach ($reqParams['work_plan_details'] as $key => $item) {
                    // new designation
                    $workPlanDetail = null;
                    if (empty($item['id'])) {
                        $workPlanDetail = new WorkPlanDetail($item);
                        $workPlanDetail->work_plan_id = $workPlan->id;
                        $workPlanDetail->save();
                    } else {
                        $workPlanDetail = WorkPlanDetail::find($item['id']);
                        if ($workPlanDetail) {
                            $workPlanDetail->update($item);
                        }
                    }
                    $workPlanDetailIds[] = $workPlanDetail->id;
                }
            }
            if(count($workPlanDetailIds) > 0)
            {
                DB::statement("UPDATE work_plan_details SET deleted_at='".Carbon::now()->format("Y-m-d H:i:s")."' WHERE work_plan_id={$workPlanDetail->id} && id not in (". implode( ',', $workPlanDetailIds).")");
            }
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [];
        $workPlan = WorkPlan::with(["user", "user.zone", "work_plan_details"])->find($id);
        if (!$workPlan) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $workPlan;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $workPlanRules = [
            'id' => 'required',
            'user_id' => 'required',
            'role_type' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'month' => 'required',
            'year' => 'required',
            'work_plan_details' => 'required',
            'work_plan_details.*.date' => 'required',
            'work_plan_details.*.revised_date' => 'required',
            'work_plan_details.*.work_type' => 'required',
            'work_plan_details.*.doctor' => 'required',
            'work_plan_details.*.morning_area' => 'required',
            'work_plan_details.*.morning_time' => 'required',
            'work_plan_details.*.evening_area' => 'required',
            'work_plan_details.*.evening_time' => 'required'
        ];
    
        $errorMessages = [
            'id.required' => 'ID is Required.',
            'user_id.required' => 'User ID Name is Required.',
            'role_type.required' => 'Role Type is Required.',
            'from_date.required' => 'From Date is Required.',
            'to_date.required' => 'To Date is Required.',
            'month.required' => 'Month is Required.',
            'year.required' => 'Year is Required.',
            'work_plan_details' => 'Work Plan Detail is Required',
            'work_plan_details.*.date' => 'Date is Required',
            'work_plan_details.*.revised_date' => 'Revised Date is Required',
            'work_plan_details.*.work_type' => 'Work Type is Required',
            'work_plan_details.*.doctor' => 'Doctor is Required',
            'work_plan_details.*.morning_area' => 'Morning Area is Required',
            'work_plan_details.*.morning_time' => 'Morning Time is Required',
            'work_plan_details.*.evening_area' => 'Evening Area is Required',
            'work_plan_details.*.evening_time' => 'Evening Time is Required'
        ];

        if(array_key_exists('isPresent', $reqParams)) {
            $workPlan = WorkPlan::find($reqParams['id']);
            $workPlan->isPresent = $reqParams['isPresent'];
            $workPlan->save();
        } else {
            $workPlanValidator = Validator::make($reqParams, $workPlanRules, $errorMessages);
            if ($workPlanValidator->fails()) {
                $response['errors'] = ArrayHelper::dotToArray($workPlanValidator->errors()->getMessages());
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            };
    
            // create database record
            try {
                $workPlan = WorkPlan::find($reqParams['id']);
                $workPlan->update($reqParams);
                $workPlanDetailIds = [];
                if (array_key_exists('work_plan_details', $reqParams)) {
                    foreach ($reqParams['work_plan_details'] as $key => $item) {
                        // new designation
                        $workPlanDetail = null;
                        if (empty($item['id'])) {
                            $workPlanDetail = new WorkPlanDetail($item);
                            $workPlanDetail->work_plan_id = $workPlan->id;
                            $workPlanDetail->save();
                        } else {
                            $workPlanDetail = WorkPlanDetail::find($item['id']);
                            if ($workPlanDetail) {
                                $workPlanDetail->update($item);
                            }
                        }
                        $workPlanDetailIds[] = $workPlanDetail->id;
                    }
                }
                if(count($workPlanDetailIds) > 0)
                {
                    DB::statement("UPDATE work_plan_details SET deleted_at='".Carbon::now()->format("Y-m-d H:i:s")."' WHERE work_plan_id={$workPlanDetail->id} && id not in (". implode( ',', $workPlanDetailIds).")");
                }
            } catch (\Exception $e) {
                report($e);
                $response['errors']['general'] = [$e->getMessage()];
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
