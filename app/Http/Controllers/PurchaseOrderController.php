<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['purchase_orders.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = PurchaseOrder::with(["company", "user", "zone", "product_units"])->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $purchaseOrderRules = [
            'user_id' => 'required',
            'zone_id' => 'required',
            'distributer_area' => 'required',
            'order_date' => 'required',
            'company_id' => 'required',
            'product_units' => 'required',
            'product_units.*.product_id' => 'required',
            'product_units.*.product_name' => 'required',
            'product_units.*.order_unit' => 'required',
            'product_units.*.price' => 'required',
        ];
    
        $errorMessages = [
            'user_id.required' => 'User ID Name is Required.',
            'zone_id.required' => 'Zone ID is Required.',
            'distributer_area.required' => 'Distributer Area is Required.',
            'order_date.required' => 'Order Date is Required.',
            'company_id.required' => 'Company ID is Required.',
            'product_units.required' => 'Product Units is Required.',
            'product_units.*.product_id' => 'Product ID is Required',
            'product_units.*.product_id' => 'Product Name is Required',
            'product_units.*.order_unit' => 'Order Unit is Required',
            'product_units.*.price' => 'Product Price is Required'
        ];

        $purchaseOrderValidator = Validator::make($reqParams, $purchaseOrderRules, $errorMessages);
        if ($purchaseOrderValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($purchaseOrderValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $purchaseOrder = new PurchaseOrder($reqParams);
            $purchaseOrder->save();
            $purchaseOrderDetailIds = [];
            if (array_key_exists('product_units', $reqParams)) {
                foreach ($reqParams['product_units'] as $key => $item) {
                    // new designation
                    $purchaseOrderDetail = null;
                    $amount = $item['price'] * $item['order_unit'];
                    if (empty($item['id'])) {
                        $purchaseOrderDetail = new PurchaseOrderDetail($item);
                        $purchaseOrderDetail->purchase_order_id = $purchaseOrder->id;
                        $purchaseOrderDetail->amount = $amount;
                        $purchaseOrderDetail->save();
                    } else {
                        $purchaseOrderDetail = PurchaseOrderDetail::find($item['id']);
                        if ($purchaseOrderDetail) {
                            $purchaseOrderDetail->amount = $amount;
                            $purchaseOrderDetail->update($item);
                        }
                    }
                    $purchaseOrderDetailIds[] = $purchaseOrderDetail->id;
                }
            }
            if(count($purchaseOrderDetailIds) > 0)
            {
                DB::statement("UPDATE purchase_order_details SET deleted_at='".Carbon::now()->format("Y-m-d H:i:s")."' WHERE purchase_order_id={$purchaseOrderDetail->id} && id not in (". implode( ',', $purchaseOrderDetailIds).")");
            }
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [];
        $purchaseOrder = PurchaseOrder::with(["company", "user", "user.zone", "product_units"])->find($id);
        if (!$purchaseOrder) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $purchaseOrder;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $purchaseOrderRules = [
            'id' => 'required',
            'user_id' => 'required',
            'zone_id' => 'required',
            'distributer_area' => 'required',
            'order_date' => 'required',
            'company_id' => 'required',
            'product_units' => 'required',
            'product_units.*.product_id' => 'required',
            'product_units.*.product_name' => 'required',
            'product_units.*.order_unit' => 'required',
            'product_units.*.price' => 'required',
        ];
    
        $errorMessages = [
            'id.required' => 'ID is Required.',
            'user_id.required' => 'User ID Name is Required.',
            'zone_id.required' => 'Zone ID is Required.',
            'distributer_area.required' => 'Distributer Area is Required.',
            'order_date.required' => 'Order Date is Required.',
            'company_id.required' => 'Company ID is Required.',
            'product_units.required' => 'Product Units is Required.',
            'product_units.*.product_id' => 'Product ID is Required',
            'product_units.*.product_id' => 'Product Name is Required',
            'product_units.*.order_unit' => 'Order Unit is Required',
            'product_units.*.price' => 'Product Price is Required'
        ];

        $purchaseOrderValidator = Validator::make($reqParams, $purchaseOrderRules, $errorMessages);
        if ($purchaseOrderValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($purchaseOrderValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $purchaseOrder = PurchaseOrder::find($reqParams['id']);
            $purchaseOrder->update($reqParams);
            $purchaseOrderDetailIds = [];
            if (array_key_exists('product_units', $reqParams)) {
                foreach ($reqParams['product_units'] as $key => $item) {
                    // new designation
                    $purchaseOrderDetail = null;
                    $amount = $item['price'] * $item['order_unit'];
                    if (empty($item['id'])) {
                        $purchaseOrderDetail = new PurchaseOrderDetail($item);
                        $purchaseOrderDetail->purchase_order_id = $purchaseOrder->id;
                        $purchaseOrderDetail->amount = $amount;
                        $purchaseOrderDetail->save();
                    } else {
                        $purchaseOrderDetail = PurchaseOrderDetail::find($item['id']);
                        if ($purchaseOrderDetail) {
                            $purchaseOrderDetail->amount = $amount;
                            $purchaseOrderDetail->update($item);
                        }
                    }
                    $purchaseOrderDetailIds[] = $purchaseOrderDetail->id;
                }
            }
            if(count($purchaseOrderDetailIds) > 0)
            {
                DB::statement("UPDATE purchase_order_details SET deleted_at='".Carbon::now()->format("Y-m-d H:i:s")."' WHERE purchase_order_id={$purchaseOrderDetail->id} && id not in (". implode( ',', $purchaseOrderDetailIds).")");
            }
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
