<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['id', 'username', 'name', 'email', 'role_type', 'zone_id', 'company_id', 'designation', 'cnic', 'phone_number', 'address', 'salary', 'status'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = User::with("company:id,name","zone:id,zone,area_name")->select($select)->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $select = ['id', 'username', 'name', 'email', 'role_type', 'zone_id', 'company_id', 'designation', 'cnic', 'phone_number', 'address', 'salary', 'status'];
        $response = [];
        $company = User::with("company:id,name","zone:id,zone,area_name")->find($id);
        if (!$company) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $company;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->all();

        $companyRules = [
            'name' => 'required|string',
            // 'email' => 'required|string|unique:users,email',
            'username' => 'required|string|unique:users',
            'role_type' => 'required|integer',
            'cnic' => 'required|integer',
            'designation' => 'required|string',
            'phone_number' => 'required|string',
            'address' => 'required|string',
            'salary' => 'required|number',
            'zone_id' => 'required|number',
            'company_id' => 'required|number',
            'status' => 'required|boolean',
        ];

        $errorMessages = [
            'id.required'         => 'Id is Required',
            'name.required' => 'Name is Required.',
            'username.required' => 'User Name is Required.',
            'role_type.required' => 'Name is Required.',
            'cnic.required' => 'CNIC is Required.',
            'designation.required' => 'Designation is Required.',
            'phone_number.required' => 'Phone Number is Required.',
            'address.required' => 'Address is Required.',
            'salary.required' => 'Salary is Required.',
            'zone_id.required' => 'Zone are Required.',
            'company_id.required' => 'Company is Required.',
            'status.required' => 'Status is Required.',
        ];

        $companyValidator = Validator::make($reqParams, $companyRules, $errorMessages);

        if ($companyValidator->fails()) {
            $response['message']['errors'] = ArrayHelper::dotToArray($companyValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            User::find($reqParams['id'])->update($reqParams);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $e) {
            $response['message']['errors']['general'][] = [$e->getMessage()];
            return response()->json($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
