<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\TargetAssignment;
use App\Models\TargetAssignmentDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TargetAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['target_assignments.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = TargetAssignment::with(["company", "user", "zone", "target_assignment_details"])->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTargetAssignmentDetail(Request $request)
    {
        extract(array_filter($request->all()));

        $query = DB::select("SELECT target_assignments.company_id, target_assignments.zone_id, target_assignments.year, target_assignments.month, target_assignment_details.product_name, target_assignment_details.employee_name, SUM(target_assignment_details.order_unit) as order_unit FROM target_assignment_details JOIN target_assignments ON target_assignments.id=target_assignment_details.target_assignment_id WHERE company_id='".$companyId."' AND zone_id='".$zoneId."' AND year='".$year."' AND month='".$month."' GROUP BY company_id, zone_id, employee_name, product_name, year, month");
        
        return response()->json($query, Response::HTTP_OK);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $targetAssignmentRules = [
            'user_id' => 'required',
            'company_id' => 'required',
            'zone_id' => 'required',
            'year' => 'required',
            'month' => 'required',
            'target_assignment_details' => 'required',
            'target_assignment_details.*.product_id' => 'required',
            'target_assignment_details.*.product_name' => 'required',
            'target_assignment_details.*.employee_id' => 'required',
            'target_assignment_details.*.employee_name' => 'required',
            'target_assignment_details.*.order_unit' => 'required'
        ];
    
        $errorMessages = [
            'user_id.required' => 'User ID Name is Required.',
            'company_id.required' => 'Company ID is Required.',
            'zone_id.required' => 'Zone ID is Required.',
            'year.required' => 'Year is Required.',
            'month.required' => 'Month is Required.',
            'target_assignment_details.required' => 'Product Units is Required.',
            'target_assignment_details.*.product_id' => 'Product ID is Required',
            'target_assignment_details.*.product_id' => 'Product Name is Required',
            'target_assignment_details.*.employee_id' => 'Employee ID is Required',
            'target_assignment_details.*.employee_name' => 'Employee Name is Required',
            'target_assignment_details.*.order_unit' => 'Order Unit is Required',
        ];

        $targetAssignmentValidator = Validator::make($reqParams, $targetAssignmentRules, $errorMessages);
        if ($targetAssignmentValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($targetAssignmentValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $targetAssignment = new TargetAssignment($reqParams);
            $targetAssignment->save();
            $targetAssignmentDetailIds = [];
            if (array_key_exists('target_assignment_details', $reqParams)) {
                foreach ($reqParams['target_assignment_details'] as $key => $item) {
                    // new designation
                    $targetAssignmentDetail = null;
                    if (empty($item['id'])) {
                        $targetAssignmentDetail = new TargetAssignmentDetail($item);
                        $targetAssignmentDetail->target_assignment_id = $targetAssignment->id;
                        $targetAssignmentDetail->save();
                    } else {
                        $targetAssignmentDetail = TargetAssignmentDetail::find($item['id']);
                        if ($targetAssignmentDetail) {
                            $targetAssignmentDetail->update($item);
                        }
                    }
                    $targetAssignmentDetailIds[] = $targetAssignmentDetail->id;
                }
            }
            if(count($targetAssignmentDetailIds) > 0)
            {
                DB::statement("UPDATE target_assignment_details SET deleted_at='".Carbon::now()->format("Y-m-d H:i:s")."' WHERE target_assignment_id={$targetAssignmentDetail->id} AND id not in (". implode( ',', $targetAssignmentDetailIds).")");
            }
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [];
        $targetAssignment = TargetAssignment::with(["company", "zone", "user", "target_assignment_details"])->find($id);
        if (!$targetAssignment) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $targetAssignment;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $targetAssignmentRules = [
            'id' => 'required',
            'user_id' => 'required',
            'company_id' => 'required',
            'zone_id' => 'required',
            'year' => 'required',
            'month' => 'required',
            'target_assignment_details' => 'required',
            'target_assignment_details.*.product_id' => 'required',
            'target_assignment_details.*.product_name' => 'required',
            'target_assignment_details.*.employee_id' => 'required',
            'target_assignment_details.*.employee_name' => 'required',
            'target_assignment_details.*.order_unit' => 'required'
        ];
    
        $errorMessages = [
            'user_id.required' => 'User ID Name is Required.',
            'company_id.required' => 'Company ID is Required.',
            'zone_id.required' => 'Zone ID is Required.',
            'year.required' => 'Year is Required.',
            'month.required' => 'Month is Required.',
            'target_assignment_details.required' => 'Product Units is Required.',
            'target_assignment_details.*.product_id' => 'Product ID is Required',
            'target_assignment_details.*.product_id' => 'Product Name is Required',
            'target_assignment_details.*.employee_id' => 'Employee ID is Required',
            'target_assignment_details.*.employee_name' => 'Employee Name is Required',
            'target_assignment_details.*.order_unit' => 'Order Unit is Required',
        ];

        $targetAssignmentValidator = Validator::make($reqParams, $targetAssignmentRules, $errorMessages);
        if ($targetAssignmentValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($targetAssignmentValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $targetAssignment = TargetAssignment::find($reqParams['id']);
            $targetAssignment->update($reqParams);
            $targetAssignmentDetailIds = [];
            if (array_key_exists('target_assignment_details', $reqParams)) {
                foreach ($reqParams['target_assignment_details'] as $key => $item) {
                    // new designation
                    $targetAssignmentDetail = null;
                    if (empty($item['id'])) {
                        $targetAssignmentDetail = new TargetAssignmentDetail($item);
                        $targetAssignmentDetail->target_assignment_id = $targetAssignment->id;
                        $targetAssignmentDetail->save();
                    } else {
                        $targetAssignmentDetail = TargetAssignmentDetail::find($item['id']);
                        if ($targetAssignmentDetail) {
                            $targetAssignmentDetail->update($item);
                        }
                    }
                    $targetAssignmentDetailIds[] = $targetAssignmentDetail->id;
                }
            }
            if(count($targetAssignmentDetailIds) > 0)
            {
                DB::statement("UPDATE target_assignment_details SET deleted_at='".Carbon::now()->format("Y-m-d H:i:s")."' WHERE target_assignment_id={$targetAssignmentDetail->id} AND id not in (". implode( ',', $targetAssignmentDetailIds).")");
            }
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
