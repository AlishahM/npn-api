<?php

namespace App\Http\Controllers;

use App\Helpers\DatabaseQueryHelper;
use App\Models\SaleExpenseProduct;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SaleExpenseProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['sale_expense_products.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = SaleExpenseProduct::with(["sale_expense:id,company_id,user_id", "sale_expense.company:id,name", "sale_expense.user:id,name,zone_id", "sale_expense.user.zone:id,zone,area_name"])->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }
}
