<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['companies.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = Company::with("zones.zone:id,zone,area_name")->orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $companyRules = [
            'name' => 'required',
            'zone_id' => 'required'
        ];
    
        $errorMessages = [
            'name.required' => 'Company Name is Required.',
            'zone_id.required' => 'Zones are Required.',
        ];

        $companyValidator = Validator::make($reqParams, $companyRules, $errorMessages);
        if ($companyValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($companyValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $company = new Company($reqParams);
            $company->save();
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [];
        $company = Company::find($id);
        if (!$company) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $company;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->all();

        $companyRules = [
            'id' => 'required',
            'name'  => 'required',
            'zone_id' => 'required',
        ];

        $errorMessages = [
            'id.required'         => 'Id is Required',
            'name.required' => 'Company Name is Required.',
            'zone_id.required' => 'Zone are Required.',
        ];

        $companyValidator = Validator::make($reqParams, $companyRules, $errorMessages);

        if ($companyValidator->fails()) {
            $response['message']['errors'] = ArrayHelper::dotToArray($companyValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            Company::find($reqParams['id'])->update($reqParams);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $e) {
            $response['message']['errors']['general'][] = [$e->getMessage()];
            return response()->json($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
