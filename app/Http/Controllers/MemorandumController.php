<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\Memorandum;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class MemorandumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['memorandums.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = Memorandum::orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $productRules = [
            'date' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ];
    
        $errorMessages = [
            'date.required' => 'Date is Required.',
            'subject.required' => 'Subject is Required.',
            'message.required' => 'Message is Required.',
        ];

        $productValidator = Validator::make($reqParams, $productRules, $errorMessages);
        if ($productValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($productValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $product = new Memorandum($reqParams);
            $product->save();
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [];
        $product = Memorandum::find($id);
        if (!$product) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $product;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->all();

        $productRules = [
            'id' => 'required',
            'date'  => 'required',
            'subject' => 'required',
            'message' => 'required',
        ];

        $errorMessages = [
            'id.required'         => 'Id is Required',
            'date.required'       => 'Date is Required.',
            'subject.required'      => 'Subject is Required.',
            'message.required' => 'Message is Required.',
        ];

        $productValidator = Validator::make($reqParams, $productRules, $errorMessages);

        if ($productValidator->fails()) {
            $response['message']['errors'] = ArrayHelper::dotToArray($productValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            Memorandum::find($reqParams['id'])->update($reqParams);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $e) {
            $response['message']['errors']['general'][] = [$e->getMessage()];
            return response()->json($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
