<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\DatabaseQueryHelper;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ZoneController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 1;
        $pageSize = 10;
        $sortBy = 'id';
        $sortOrder = 'asc';
        $select = ['zones.*'];
        $filters = null;
        extract(array_filter($request->all()));

        $query = Zone::orderBy($sortBy, $sortOrder);
        $query = DatabaseQueryHelper::filter($query, $filters);

        if ($page==-1)
            return response()->json($query->get(), Response::HTTP_OK);
        else
            return response()->json($query->paginate($pageSize, $select, 'page', $page), Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [];
        $reqParams = $request->json()->all();

        // validate request
        $zoneRules = [
            'zone' => 'required',
            'area_name' => 'required'
        ];
    
        $errorMessages = [
            'zone.required' => 'Zone Name is Required.',
            'area_name.required' => 'Area Name is Required.',
        ];

        $zoneValidator = Validator::make($reqParams, $zoneRules, $errorMessages);
        if ($zoneValidator->fails()) {
            $response['errors'] = ArrayHelper::dotToArray($zoneValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        };

        // create database record
        try {
            $zone = new Zone($reqParams);
            $zone->save();
        } catch (\Exception $e) {
            report($e);
            $response['errors']['general'] = [$e->getMessage()];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [];
        $zone = Zone::find($id);
        if (!$zone) {
            $response['error']['id'] = 'Invalid Id.';
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = $zone;
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response = [];
        $reqParams = $request->all();

        $zoneRules = [
            'id' => 'required',
            'zone'  => 'required',
            'area_name' => 'required',
        ];

        $errorMessages = [
            'id.required'         => 'Id is Required',
            'zone.required' => 'Zone Name is Required.',
            'area_name.required' => 'Area Name is Required.',
        ];

        $zoneValidator = Validator::make($reqParams, $zoneRules, $errorMessages);

        if ($zoneValidator->fails()) {
            $response['message']['errors'] = ArrayHelper::dotToArray($zoneValidator->errors()->getMessages());
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            Zone::find($reqParams['id'])->update($reqParams);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $e) {
            $response['message']['errors']['general'][] = [$e->getMessage()];
            return response()->json($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
