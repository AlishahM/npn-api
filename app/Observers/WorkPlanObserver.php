<?php

namespace App\Observers;

use App\Http\Controllers\SequenceNoController;
use App\Models\WorkPlan;

class WorkPlanObserver
{
    /**
     * Handle the WorkPlan "creating" event.
     *
     * @param  \App\Models\WorkPlan  $workPlan
     * @return void
     */
    public function creating(WorkPlan $workPlan)
    {
        $workPlan->plan_no = SequenceNoController::getWorkPlanNo();
    }

    /**
     * Handle the WorkPlan "created" event.
     *
     * @param  \App\Models\WorkPlan  $workPlan
     * @return void
     */
    public function created(WorkPlan $workPlan)
    {
        SequenceNoController::updateWorkPlanNo();
    }

    /**
     * Handle the WorkPlan "created" event.
     *
     * @param  \App\Models\WorkPlan  $workPlan
     * @return void
     */
    public function saved(WorkPlan $workPlan)
    {
        //
    }

    /**
     * Handle the WorkPlan "updated" event.
     *
     * @param  \App\Models\WorkPlan  $workPlan
     * @return void
     */
    public function updated(WorkPlan $workPlan)
    {
        //
    }

    /**
     * Handle the WorkPlan "deleted" event.
     *
     * @param  \App\Models\WorkPlan  $workPlan
     * @return void
     */
    public function deleted(WorkPlan $workPlan)
    {
        //
    }

    /**
     * Handle the WorkPlan "restored" event.
     *
     * @param  \App\Models\WorkPlan  $workPlan
     * @return void
     */
    public function restored(WorkPlan $workPlan)
    {
        //
    }

    /**
     * Handle the WorkPlan "force deleted" event.
     *
     * @param  \App\Models\WorkPlan  $workPlan
     * @return void
     */
    public function forceDeleted(WorkPlan $workPlan)
    {
        //
    }
}
