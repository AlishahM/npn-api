<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SequenceNoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateNow = Carbon::now()->toDateTimeString();
        DB::table('sequence_nos')->delete();

        $sequence_nos = array(
            array('id' => 1, 'purchase_order' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow)
        );

        DB::table('sequence_nos')->insert($sequence_nos);
    }
}
