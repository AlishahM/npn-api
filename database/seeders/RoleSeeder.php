<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateNow = Carbon::now()->toDateTimeString();
        DB::table('roles')->delete();

        $roles = array(
            array('id' => 1, 'role' => 'Admin', 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 2, 'role' => 'Employee', 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 3, 'role' => 'Distributer', 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 4, 'role' => 'Manager', 'created_at'=> $dateNow, 'updated_at'=> $dateNow)
        );

        DB::table('roles')->insert($roles);
    }
}
