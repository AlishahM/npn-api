<?php

namespace Database\Seeders;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateNow = Carbon::now()->toDateTimeString();
        DB::table('products')->delete();

        $products = array(
            array('id' => 1, 'name' => 'NEOLAC-1 (400g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 2, 'name' => 'NEOLAC-2 (400g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 3, 'name' => 'NEOLACRISE-3 (400g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 4, 'name' => 'NEOLAC PREMATURE (300g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 5, 'name' => 'F-LAC (300g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 6, 'name' => 'PRO-EN (400g)', 'price' => 1349, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 7, 'name' => 'RICE CEREALS (200g)', 'price' => 367, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 8, 'name' => '5 MIX & 3 FRUIT (200g)', 'price' => 367, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 9, 'name' => 'ECD-1 (400g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 10, 'name' => 'ECD-2 (400g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 11, 'name' => 'ECD-GROW (400g)', 'price' => 1198, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 12, 'name' => 'GAULLAC-1 (200g)', 'price' => 460, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 13, 'name' => 'GAULLAC-2 (200g)', 'price' => 460, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 14, 'name' => 'GAULLACRISE-3 (200g)', 'price' => 460, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 15, 'name' => 'GAULLAC-LF (200g)', 'price' => 560, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
            array('id' => 16, 'name' => 'F-Lac (350g)', 'price' => 1274, 'company_id' => 1, 'created_at'=> $dateNow, 'updated_at'=> $dateNow),
        );

        DB::table('products')->insert($products);

    }
}
