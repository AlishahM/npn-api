<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->id();
            $table->string("po_no")->nullable(false);
            $table->integer("user_id")->nullable(false);
            $table->integer("zone_id")->nullable(false);
            $table->string("distributer_area")->nullable(false);
            $table->string("order_date")->nullable(false);
            $table->string("company_id")->nullable(false);
            $table->boolean("is_accepted")->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
