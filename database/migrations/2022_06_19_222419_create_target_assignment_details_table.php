<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetAssignmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_assignment_details', function (Blueprint $table) {
            $table->id();
            $table->integer("target_assignment_id")->nullable(false);
            $table->integer("product_id")->nullable(false);
            $table->string("product_name")->nullable(false);
            $table->integer("employee_id")->nullable(false);
            $table->string("employee_name")->nullable(false);
            $table->integer("order_unit")->nullable(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_assignment_details');
    }
}
