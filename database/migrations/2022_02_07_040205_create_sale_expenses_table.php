<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_expenses', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id");
            $table->integer("role_type");
            $table->integer("company_id");
            $table->integer("total_unit_sold");
            $table->integer("total_sale_value");
            $table->integer("total_month_expense");
            $table->integer("total_annual_expense");
            $table->integer("total_claim_expense");
            $table->integer("total_promotional_expense");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_expenses');
    }
}
