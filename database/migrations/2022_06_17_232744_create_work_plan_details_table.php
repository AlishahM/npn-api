<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkPlanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_plan_details', function (Blueprint $table) {
            $table->id();
            $table->integer("work_plan_id")->nullable(false);
            $table->date("date")->nullable(false);
            $table->date("revised_date")->nullable(false);
            $table->integer("work_type")->nullable(false);
            $table->string("morning_area")->nullable(false);
            $table->time("morning_time")->nullable(false);
            $table->string("evening_area")->nullable(false);
            $table->time("evening_time")->nullable(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_plan_details');
    }
}
