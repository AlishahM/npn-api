<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_assignments', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id")->nullable(false);
            $table->integer("role_type")->nullable(false);
            $table->integer("company_id")->nullable(false);
            $table->integer("zone_id")->nullable(false);
            $table->year("year")->nullable(false);
            $table->integer("month")->nullable(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_assignments');
    }
}
