<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributorLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_ledgers', function (Blueprint $table) {
            $table->id();
            $table->string('zone');
            $table->string('distributorName');
            $table->integer('quantity')->nullable(true);
            $table->integer('totalAmount')->nullable(true);
            $table->date("dopr")->nullable(true);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributor_ledgers');
    }
}
