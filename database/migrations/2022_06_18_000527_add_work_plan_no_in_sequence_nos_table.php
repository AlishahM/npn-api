<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWorkPlanNoInSequenceNosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sequence_nos', function (Blueprint $table) {
            $table->integer("work_plan_no")->after("purchase_order")->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sequence_nos', function (Blueprint $table) {
            $table->removeColumn("work_plan_no");
        });
    }
}
