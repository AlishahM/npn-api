<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_plans', function (Blueprint $table) {
            $table->id();
            $table->string("plan_no")->nullable(false);
            $table->integer("user_id")->nullable(false);
            $table->integer("role_type")->nullable(false);
            $table->date("from_date")->nullable(false);
            $table->date("to_date")->nullable(false);
            $table->integer("month")->nullable(false);
            $table->integer("year")->nullable(false);
            $table->boolean("isPresent")->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_plans');
    }
}
